export function getContrats() {
  return Promise.resolve([
    {
      id: "59d8f4a3-3553-4950-9d0c-2f3ff4488e00",
      title: "ARS",
      codeProduit: 32,
      status: "EN_COURS",
      repartition: "40/60",
      dateOuverture: "2020-06-01",
      dateDeregulation: "2024-06-01",
      mouvements: [
        { date: "2022-10-01", description: "Versement libre" },
        { date: "2023-01-01", description: "Reajustement annuel" },
      ],
    },
    {
      id: "a4551590-1875-409a-8200-2edbeb31697c",
      title: "ARS",
      codeProduit: 32,
      status: "RESILIE",
      repartition: "80/20",
      dateOuverture: "2015-06-01",
      dateDeregulation: "2019-06-01",
      dateResiliation: "2022-12-21",
      mouvements: [
        { date: "2016-01-01", description: "Reajustement annuel" },
        { date: "2017-01-01", description: "Reajustement annuel" },
        { date: "2018-01-01", description: "Reajustement annuel" },
        { date: "2019-01-01", description: "Reajustement annuel" },
        { date: "2020-01-01", description: "Reajustement annuel" },
        { date: "2021-01-01", description: "Reajustement annuel" },
        { date: "2022-01-01", description: "Reajustement annuel" },
        {
          date: "2022-12-21",
          description: "Versement sur compte suite à résiliation",
        },
      ],
    },

    {
      id: "73fefbdf-b7a9-4819-a836-134508f1d3b8",
      title: "SER",
      codeProduit: 25,
      status: "EN_COURS",
      pourcentageActuel: "100%",
      dateDeregulation: "2024-06-01",
      mouvements: [],
    },
  ]);
}

export function getContratsNew() {
  return Promise.resolve([
    {
      id: "c749847f-1be5-49d8-ba48-dcf51bae14b1",
      title: "ARS",
      codeProduit: 32,
      status: "EN_COURS",
      repartition: "40/60",
      dateOuverture: "2020-06-01",
      dateDeregulation: "2024-06-01",
      mouvements: [
        { date: "2022-10-01", description: "Versement libre" },
        { date: "2023-01-01", description: "Reajustement annuel" },
      ],
    },
    {
      id: "33461450-0cc9-471e-9301-365c26a6b51d",
      title: "ARS",
      codeProduit: 32,
      status: "RESILIE",
      repartition: "80/20",
      dateOuverture: "2015-06-01",
      dateDeregulation: "2019-06-01",
      dateResiliation: "2022-12-21",
      mouvements: [
        { date: "2016-01-01", description: "Reajustement annuel" },
        { date: "2017-01-01", description: "Reajustement annuel" },
        { date: "2018-01-01", description: "Reajustement annuel" },
        { date: "2019-01-01", description: "Reajustement annuel" },
        { date: "2020-01-01", description: "Reajustement annuel" },
        { date: "2021-01-01", description: "Reajustement annuel" },
        { date: "2022-01-01", description: "Reajustement annuel" },
        {
          date: "2022-12-21",
          description: "Versement sur compte suite à résiliation",
        },
      ],
    },

    {
      id: "9924d5bb-96b9-417d-a486-46624608ca54",
      title: "SER",
      codeProduit: 25,
      status: "EN_COURS",
      pourcentageActuel: "100%",
      dateDeregulation: "2024-06-01",
      mouvements: [],
    },

    {
      id: "bbf51e97-e46b-4fa5-be16-5e23d21e0ee2",
      title: "RCAPSOL",
      typeProduit: "PREVOYANCE",
      codeProduit: 99,
      status: "EN_COURS",
      montant: "55000",
      intentation: "proteger ma famille",
      dateCreation: "2023-05-01",
      beneficiaires: ["Vincent", "Josette"],
    },

    {
      id: "7fc25b6c-0cbc-4ee7-bef3-17a461b94981",
      title: "RCAPSOL",
      typeProduit: "PREVOYANCE",
      codeProduit: 99,
      status: "RESILIE",
      montant: "20000",
      intentation: "proteger ma famille",
      dateCreation: "2020-05-23",
      dateResiliation: "2022-08-15",
      beneficiaires: ["Vincent", "Josette", "Hervé"],
    },
  ]);
}
