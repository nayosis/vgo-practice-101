import { Segmented } from "antd";
import React, { useEffect, useState } from "react";
import Contrats from "./component/Contrats";
import { getContrats, getContratsNew } from "./service/contratServices.js";
import "./index.css";

export function App(props) {
  const [contratsV1, setContratsV1] = useState([]);
  const [contratsV2, setContratsV2] = useState([]);
  const [contrats, setContrats] = useState([]);

  useEffect(() => {
    getContrats().then((innerContrats) => {
      setContratsV1(innerContrats);
      setContrats(innerContrats);
    });

    getContratsNew().then((innerContrats) => setContratsV2(innerContrats));
  }, []);

  function switchVersionContrat(value) {
    if (value === "ContratV1") {
      setContrats(contratsV1);
    } else {
      setContrats(contratsV2);
    }
  }

  return (
    <div className="App">
      <h2> App Gestion Contrat </h2>
      <Segmented
        options={["ContratV1", "ContratV2"]}
        onChange={(value) => switchVersionContrat(value)}
      />

      <Contrats contrats={contrats} />
    </div>
  );
}

export default App;
