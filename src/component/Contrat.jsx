import React from "react";
import {
  Button,
  Badge,
  Card,
  Divider,
  Timeline,
  Descriptions,
  Space,
  List,
  Tag,
} from "antd";

const Contrat = ({ contrat }) => {
  return (
    <Badge.Ribbon text="EPARGNE" color="purple">
      <Card className="contrat" title={`Contrat ${contrat.title}`}>
        <Descriptions title="Informations">
          <Descriptions.Item label="Status">
            <Tag color="#f50">{contrat?.status}</Tag>
          </Descriptions.Item>
          {contrat?.codeProduit === 32 && (
            <Descriptions.Item label="Repartition">
              {contrat?.repartition}
            </Descriptions.Item>
          )}

          {contrat?.codeProduit === 25 && (
            <Descriptions.Item label="Pourcentage">
              {contrat?.pourcentageActuel}
            </Descriptions.Item>
          )}

          {contrat?.codeProduit === 25 ? (
            <Descriptions.Item label="Date de fin de regulation">
              Date + 15 ans
            </Descriptions.Item>
          ) : (
            <Descriptions.Item label="Date de fin de regulation">
              {contrat?.dateDeregulation}
            </Descriptions.Item>
          )}

          {contrat?.dateResiliation && (
            <Descriptions.Item label="Resiliation">
              {contrat?.dateResiliation}
            </Descriptions.Item>
          )}
        </Descriptions>
        {contrat?.codeProduit !== 25 && (
          <Timeline
            items={contrat?.mouvements.map((mvt) => ({
              children: (
                <>
                  <strong>{mvt.date} - </strong>
                  {mvt.description}
                </>
              ),
            }))}
          />
        )}
        <Divider orientation="left"></Divider>
        <Space>
          <Button type="primary">Acceder</Button>
          <Button>Imprimer</Button>
        </Space>
      </Card>
    </Badge.Ribbon>
  );
};

export default Contrat;
