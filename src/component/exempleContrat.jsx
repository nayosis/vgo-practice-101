import React from "react";
import {
  Button,
  Badge,
  Card,
  Divider,
  Descriptions,
  Space,
  List,
  Tag,
} from "antd";

const ExempleContrat = () => {
  return (
    <Badge.Ribbon text="PREVOYANCE" color="yellow">
      <Card className="contrat" title={`Contrat RCAPSOL`}>
        <Descriptions title="Informations">
          <Descriptions.Item label="Status">
            <Tag color="#f50">EN_COURS</Tag>
          </Descriptions.Item>
          <Descriptions.Item label="Intention">
            Assurer l'avenir des enfants
          </Descriptions.Item>
          <Descriptions.Item label="montant">25000 €</Descriptions.Item>
        </Descriptions>
        <div>
          <p>Beneficiares (2)</p>
          <Tag color="#87d068">Vincent</Tag>
          <Tag color="#87d068">Roger</Tag>
        </div>
        <br />
        <Space>
          <Button type="primary">Acceder</Button>
          <Button>Imprimer</Button>
        </Space>
      </Card>
    </Badge.Ribbon>
  );
};

export default ExempleContrat;
