import Contrat from "./Contrat";
import "../index.css";

const Contrats = ({ contrats }) => {
  return (
    <>
      {contrats?.map((innerContrat) => (
        <Contrat key={innerContrat.id} contrat={innerContrat} />
      ))}
    </>
  );
};

export default Contrats;
