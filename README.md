## Practice 101

Cette mise en situation a pour but de voir danbs un context metier comment un developpeur réagit a une demande d'évolution dans un context non connu et une application avec defaut.

## Spec

### Existant

Cette application permet d'afficher des contrats Epargne.

Application existante
![alt existant](existant.png "existant")

Il existe de type de contrats ayant chacun des données.

- le contrat ARS (code produit 32 ) -> contrat epargne à repartition dynamique
- le contrat SER (code produit 25 ) -> contrat epargne fixe a montant fixe evolutif mais fixe ( ce produit n'existe pas hein ? )

_TIPS : Il s'agit d'une toile de fond, l'important est que les contrats ont des données différentes._

---

### Evolution

Notre PO vous demande d'afficher un nouveau type de contrat. En effet, notre entreprise se lance sur un nouveau contrat typé Prevoyance, le contrat RSOL code produit 99 . C'est un contrat bien different des deux deja existant, et dans un domaine bien different (Epargne versus Prevoyance)

_TIPS : La on a un contrat avec encore d'autre données et surtout dans un domaine completement different. C'est un comme un contrat d'assurance voiture et un contrat d'assurance Maison._

je souhaite donc voir dans la liste ces nouveaux contrats ( appel apiV2) avec leurs informations spécifiques

- montant
- intentation
- indicateur de type de produit

Maquette nouveau Contrat
![alt maquette](maquette.png "maquette")

---

Pour passez des données apiV1 ( exitant) a apiV2 ( new) ,

![alt api](switchV1V2.png "api")

---

**Allez plus loin** : De nouveau contrat sur d'autre produit pourrait arriver rapidement par la suite. et Il faudrait aessayer d'assurer la non regression des autres produits quand on en ajoute un nouveau ou que l on fait evoluer l'affichage d'un autre.

## C'est à vous

conseil : vous arrivez sur une application avec "des biais", n'hesitez pas à interroger le PO ( examinateur). prenez du recul sur le code existant et voyer comment vous integrer dedans.
